package com.example.todolist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.data.AppSharedPrefs
import com.example.todolist.data.ToDoItem
import com.example.todolist.databinding.ItemTodoBinding

class ToDoItemAdapter(
    private val values: MutableList<ToDoItem> = mutableListOf(),
    val onEditListener: (Long) -> Unit,
    val onDeleteListener: (Long) -> Unit
) : RecyclerView.Adapter<ToDoItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemTodoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.bind(
            item = item,
            onEditListener = onEditListener,
            onDeleteListener = onDeleteListener
        )
    }

    override fun getItemCount(): Int = values.size

    fun updateValues(newValues: List<ToDoItem>) {
        val calculateDiff = DiffUtil.calculateDiff(TodoItemCallback(values, newValues))
        values.clear()
        values.addAll(newValues)
        calculateDiff.dispatchUpdatesTo(this)
    }

    inner class ViewHolder(private val binding: ItemTodoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            with(binding) {
                if (checkbox.isChecked) {
                    textItem.setTextColor(AppSharedPrefs.getColorScheme().checkedColor)
                } else {
                    textItem.setTextColor(AppSharedPrefs.getColorScheme().uncheckedColor)
                }
            }
        }

        fun bind(
            item: ToDoItem,
            onEditListener: (Long) -> Unit,
            onDeleteListener: (Long) -> Unit
        ) = with(binding) {
            textItem.text = item.text
            deleteBtn.setOnClickListener {
                onDeleteListener(item.id)
            }
            editBtn.setOnClickListener {
                onEditListener(item.id)
            }
            checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                if (checkbox.isChecked) {
                    textItem.setTextColor(AppSharedPrefs.getColorScheme().checkedColor)
                } else {
                    textItem.setTextColor(AppSharedPrefs.getColorScheme().uncheckedColor)
                }
            }
        }
    }
}