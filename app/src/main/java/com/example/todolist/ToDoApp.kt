package com.example.todolist

import android.app.Application
import com.example.todolist.data.AppSharedPrefs

class ToDoApp : Application() {

    override fun onCreate() {
        super.onCreate()
        AppSharedPrefs.init(this)
    }
}