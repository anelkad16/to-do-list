package com.example.todolist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.todolist.adapter.ToDoItemAdapter
import com.example.todolist.data.AppDatabase
import com.example.todolist.data.ToDoItem
import com.example.todolist.databinding.FragmentListDetailsBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListDetailsFragment : Fragment() {

    private val adapter = ToDoItemAdapter(
        onEditListener = ::showEditDialog,
        onDeleteListener = ::showDeleteDialog
    )
    private lateinit var binding: FragmentListDetailsBinding
    private val database by lazy { AppDatabase.getDatabase(requireContext()) }

    companion object {
        @JvmStatic
        fun newInstance(listId: Long, title: String) =
            ListDetailsFragment().apply {
                arguments = Bundle().apply {
                    putLong("listId", listId)
                    putString("title", title)
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = binding.list
        recyclerView.adapter = adapter
        refreshTodoItems()
        binding.apply {
            toolbar.title = arguments?.getString("title")
            toolbar.setNavigationOnClickListener {
                parentFragmentManager.popBackStack()
            }
            floatingActionBtn.setOnClickListener {
                showAddListDialogMaterial()
            }
        }
    }

    private fun showAddListDialogMaterial() {
        val editText = EditText(requireContext()).also {
            it.hint = "Enter text"
        }
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Add new item")
            .setView(editText)
            .setPositiveButton("Add") { dialog, _ ->
                onToDoItemAdded(editText.text.toString())
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun onDelete(itemId: Long) {
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                database.itemDao().delete(itemId)
            }
            refreshTodoItems()
            Toast.makeText(requireContext(), "Todo item deleted", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onEdit(itemId: Long, textForNewItem: String) {
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                database.itemDao().update(itemId, textForNewItem)
            }
            refreshTodoItems()
            Toast.makeText(requireContext(), textForNewItem, Toast.LENGTH_SHORT).show()
        }
    }

    private fun showDeleteDialog(itemId: Long) {
        MaterialAlertDialogBuilder(binding.root.context)
            .setTitle(DELETE_TITLE)
            .setMessage(CONFIRMATION_FOR_DELETE_QUESTION)
            .setPositiveButton("Delete") { dialog, _ ->
                onDelete(itemId)
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun showEditDialog(itemId: Long) {
        val editText = EditText(binding.root.context)
        editText.hint = "Enter text"
        MaterialAlertDialogBuilder(binding.root.context)
            .setTitle(EDIT_TITLE)
            .setMessage(CONFIRMATION_FOR_EDIT_QUESTION)
            .setView(editText)
            .setPositiveButton("Edit") { dialog, _ ->
                onEdit(itemId, editText.text.toString())
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun onToDoItemAdded(text: String) {
        lifecycleScope.launch {
            val newItem = ToDoItem(
                text = text,
                listId = arguments?.getLong("listId")!!
            )
            withContext(Dispatchers.IO) {
                database.itemDao().insertAll(newItem)
            }
            refreshTodoItems()
            Toast.makeText(
                requireContext(), "Item added: $text", Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun refreshTodoItems() {
        lifecycleScope.launch {
            val items = withContext(Dispatchers.IO) {
                arguments?.getLong("listId")?.let { database.itemDao().getAllItems(listId = it) }
            }
            items?.apply {
                adapter.updateValues(this)
            }
        }
    }
}