package com.example.todolist

internal const val CONFIRMATION_FOR_DELETE_QUESTION =
    "Are you sure you want to delete this item?"
internal const val CONFIRMATION_FOR_EDIT_QUESTION =
    "Are you sure you want to edit this item?"
internal const val EDIT_TITLE = "Edit item"
internal const val DELETE_TITLE = "Delete item"

interface OnToDoActonListener {
    fun showDeleteDialog(itemId: Long)
    fun showEditDialog(itemId: Long)
}
