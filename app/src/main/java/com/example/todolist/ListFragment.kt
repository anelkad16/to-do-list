package com.example.todolist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.todolist.adapter.ToDoListAdapter
import com.example.todolist.data.AppDatabase
import com.example.todolist.data.ListItem
import com.example.todolist.databinding.FragmentListItemsListBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListFragment : Fragment() {

    private val adapter = ToDoListAdapter(
        onDeleteListener = ::showDeleteDialog,
        onEditListener = ::showEditDialog,
        onItemClick = ::navigateToDetails
    )

    private lateinit var binding: FragmentListItemsListBinding
    private val database by lazy { AppDatabase.getDatabase(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListItemsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = binding.list
        recyclerView.adapter = adapter
        refreshTodoItems()
        binding.floatingActionBtn.setOnClickListener { showAddListDialogMaterial() }
        binding.btnSettings.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, SettingsFragment())
                .addToBackStack(null)
                .commit()
        }
    }

    private fun showAddListDialogMaterial() {
        val editText = EditText(requireContext()).also {
            it.hint = "Enter text"
        }
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Add new list")
            .setView(editText)
            .setPositiveButton("Add") { dialog, _ ->
                onListItemAdded(editText.text.toString())
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun onDelete(itemId: Long) {
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                database.listDao().delete(itemId)
                database.itemDao().deleteAllItems(itemId)
            }
            refreshTodoItems()
            Toast.makeText(requireContext(), "List item deleted", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onEdit(itemId: Long, textForNewItem: String) {
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                database.listDao().update(itemId, textForNewItem)
            }
            refreshTodoItems()
            Toast.makeText(requireContext(), textForNewItem, Toast.LENGTH_SHORT).show()
        }
    }

    private fun showDeleteDialog(itemId: Long) {
        MaterialAlertDialogBuilder(binding.root.context)
            .setTitle(DELETE_TITLE)
            .setMessage(CONFIRMATION_FOR_DELETE_QUESTION)
            .setPositiveButton("Delete") { dialog, _ ->
                onDelete(itemId)
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun showEditDialog(itemId: Long) {
        val editText = EditText(binding.root.context)
        editText.hint = "Enter text"
        MaterialAlertDialogBuilder(binding.root.context)
            .setTitle(EDIT_TITLE)
            .setMessage(CONFIRMATION_FOR_EDIT_QUESTION)
            .setView(editText)
            .setPositiveButton("Edit") { dialog, _ ->
                onEdit(itemId, editText.text.toString())
                dialog.dismiss()
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun refreshTodoItems() {
        lifecycleScope.launch {
            val items = withContext(Dispatchers.IO) {
                database.listDao().getAll()
            }
            adapter.updateValues(items)
        }
    }

    private fun onListItemAdded(text: String) {
        lifecycleScope.launch {
            val newItem = ListItem(
                name = text
            )
            withContext(Dispatchers.IO) {
                database.listDao().insertAll(newItem)
            }
            refreshTodoItems()
            Toast.makeText(
                requireContext(), "List added: $text", Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun navigateToDetails(listId: Long, title: String) {
        val detailsFragment = ListDetailsFragment.newInstance(listId, title)
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, detailsFragment)
            .addToBackStack(null)
            .commit()
    }
}