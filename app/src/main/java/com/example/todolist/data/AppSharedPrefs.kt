package com.example.todolist.data

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.ColorRes
import com.example.todolist.R
import kotlin.properties.Delegates.notNull

object AppSharedPrefs {

    private const val PREFS_NAME = "todo-prefs"
    private const val KEY_CHECKED = "checked"
    private const val KEY_UNCHECKED = "unchecked"

    var sharedPrefs by notNull<SharedPreferences>()
        private set

    fun init(context: Context) {
        sharedPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    fun saveCheckedColor(@ColorRes color: Int) {
        sharedPrefs.edit().putInt(KEY_CHECKED, color).apply()
    }

    fun saveUncheckedColor(@ColorRes color: Int) {
        sharedPrefs.edit().putInt(KEY_UNCHECKED, color).apply()
    }

    fun getColorScheme(): ColorSchema {
        val checkedColor = sharedPrefs.getInt(KEY_CHECKED, R.color.black)
        val uncheckedColor = sharedPrefs.getInt(KEY_UNCHECKED, R.color.black)
        return ColorSchema(
            uncheckedColor = uncheckedColor,
            checkedColor = checkedColor
        )
    }
}

data class ColorSchema(
    val uncheckedColor: Int,
    val checkedColor: Int
)