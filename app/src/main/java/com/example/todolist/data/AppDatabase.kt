package com.example.todolist.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [ToDoItem::class, ListItem::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun itemDao(): ToDoItemDao

    abstract fun listDao(): ListItemDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "todolist_database"
                ).build()
                INSTANCE = instance
                instance
            }
    }
}