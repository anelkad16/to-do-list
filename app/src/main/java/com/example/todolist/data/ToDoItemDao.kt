package com.example.todolist.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ToDoItemDao {

    @Query("SELECT * FROM todo_item_table WHERE list_id = :listId")
    fun getAllItems(listId: Long): List<ToDoItem>

    @Insert
    fun insertAll(vararg items: ToDoItem)

    @Query("DELETE FROM todo_item_table WHERE list_id = :listId")
    fun deleteAllItems(listId: Long)

    @Query("DELETE FROM todo_item_table WHERE id = :id")
    fun delete(id: Long)

    @Query("UPDATE todo_item_table SET text = :newText WHERE id = :id")
    fun update(id: Long, newText: String)
}