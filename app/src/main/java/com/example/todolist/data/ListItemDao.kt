package com.example.todolist.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ListItemDao {

    @Query("SELECT * FROM todo_list_table")
    fun getAll(): List<ListItem>

    @Insert
    fun insertAll(vararg items: ListItem)

    @Query("DELETE FROM todo_list_table WHERE id = :id")
    fun delete(id: Long)

    @Query("UPDATE todo_list_table SET name = :newName WHERE id = :id")
    fun update(id: Long, newName: String)
}