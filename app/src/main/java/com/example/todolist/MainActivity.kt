package com.example.todolist

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.data.AppDatabase
import com.example.todolist.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val recyclerViewFragment = ListFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        showTodoListFragment()
    }

    private fun showTodoListFragment() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, recyclerViewFragment)
            .commit()
    }
}

